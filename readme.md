# dotfiles

* Symlink the git and shell files
* Install applications and command-line tools for macOS
* Set custom macOS preferences

## Setup

* Clone or Download this repo
* Install XCode Command Line Tools

```
xcode-select --install
```
