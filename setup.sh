#!/bin/zsh

# Install XCode Command Line Tools
# xcode-select --install

source recipes/dotfiles.sh
source recipes/brew.sh
source recipes/brew-recipes.sh
source recipes/brew-casks.sh
source recipes/npm.sh
source recipes/composer.sh
source recipes/composer-recipes.sh
