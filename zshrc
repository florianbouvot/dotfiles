# Q pre block. Keep at the top of this file.
[[ -f "${HOME}/Library/Application Support/amazon-q/shell/zshrc.pre.zsh" ]] && builtin source "${HOME}/Library/Application Support/amazon-q/shell/zshrc.pre.zsh"
# PATH
#export PATH="/usr/local/opt/php@8.1/bin:$PATH"
#export PATH="/usr/local/opt/php@8.1/sbin:$PATH"
export PATH=$PATH:$HOME/.composer/vendor/bin
export PNPM_HOME="/Users/florianbouvot/Library/pnpm"
export PATH="$PNPM_HOME:$PATH"
export PATH="/usr/local/sbin:$PATH"

# Aliases
# if php issue run `composer global update`
#alias tweeb="pnpm install; valet link; valet secure; valet use; composer install"
#alias tweeb="pnpm install; herd secure; herd use; composer install"
alias tweeb="pnpm install; herd use; composer install"

# Get macOS Software Updates, and update installed Ruby gems, Homebrew, npm, and their installed packages
alias update="sudo softwareupdate -i -a; brew update; brew upgrade; brew cleanup; npm install npm -g; npm update -g; composer global update"
#sudo gem update --system; sudo gem update; sudo gem cleanup

# IP addresses
alias ip="dig +short myip.opendns.com @resolver1.opendns.com"
alias localip="ipconfig getifaddr en0"

# Flush DNS cache
alias flush="sudo killall -HUP mDNSResponder"

# Herd injected NVM configuration
export NVM_DIR="/Users/florianbouvot/Library/Application Support/Herd/config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

[[ -f "/Applications/Herd.app/Contents/Resources/config/shell/zshrc.zsh" ]] && builtin source "/Applications/Herd.app/Contents/Resources/config/shell/zshrc.zsh"

# Herd injected PHP binary.
export PATH="/Users/florianbouvot/Library/Application Support/Herd/bin/":$PATH

# Herd injected PHP 7.4 configuration.
export HERD_PHP_74_INI_SCAN_DIR="/Users/florianbouvot/Library/Application Support/Herd/config/php/74/"

# Herd injected PHP 8.0 configuration.
export HERD_PHP_80_INI_SCAN_DIR="/Users/florianbouvot/Library/Application Support/Herd/config/php/80/"

# Herd injected PHP 8.1 configuration.
export HERD_PHP_81_INI_SCAN_DIR="/Users/florianbouvot/Library/Application Support/Herd/config/php/81/"

# Herd injected PHP 8.2 configuration.
export HERD_PHP_82_INI_SCAN_DIR="/Users/florianbouvot/Library/Application Support/Herd/config/php/82/"

# Herd injected PHP 8.3 configuration.
export HERD_PHP_83_INI_SCAN_DIR="/Users/florianbouvot/Library/Application Support/Herd/config/php/83/"

# Herd injected PHP 8.4 configuration.
export HERD_PHP_84_INI_SCAN_DIR="/Users/florianbouvot/Library/Application Support/Herd/config/php/84/"

# Q post block. Keep at the bottom of this file.
[[ -f "${HOME}/Library/Application Support/amazon-q/shell/zshrc.post.zsh" ]] && builtin source "${HOME}/Library/Application Support/amazon-q/shell/zshrc.post.zsh"

