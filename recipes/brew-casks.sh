#!/bin/zsh

# Homebrew casks

echo "Installing Homebrew casks"

brew install --cask codewhisperer
brew install --cask dbngin
#brew install --cask discord
brew install --cask figma
brew install --cask firefox
brew install --cask google-chrome
#brew install --cask herd
#brew install --cask kdrive
brew install --cask microsoft-edge
brew install --cask nova
#brew install --cask paragon-ntfs
brew install --cask transmission
brew install --cask transmission-remote-gui
brew install --cask transmit
brew install --cask visual-studio-code
brew install --cask vlc

brew cleanup
