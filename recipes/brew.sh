#!/bin/zsh

# Homebrew

if ! command -v brew &> /dev/null; then

  echo "Installing Homebrew"
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  brew doctor

else

  echo "Updating Homebrew"
  brew update
  brew upgrade

fi
