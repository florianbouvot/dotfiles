#!/bin/zsh

# Dotfiles

echo "Symlink dotfiles"

ln -sf ~/Herd/dotfiles/zshrc ~/.zshrc
ln -sf ~/Herd/dotfiles/gitconfig ~/.gitconfig
ln -sf ~/Herd/dotfiles/gitignore_global ~/.gitignore_global
